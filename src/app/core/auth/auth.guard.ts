import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { map, tap } from 'rxjs/operators';
import { go } from '../store/actions/router.actions';
import { AppState } from '../../app.module';
import { getToken } from './auth.selector';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {

  constructor(private store: Store<AppState>) {}

  canActivate(): Observable<boolean> {
    return this.store.pipe(
      select(getToken),
      map(token => !!token),
      tap(isLogged => {
        if (!isLogged) {
          this.store.dispatch(go({ path: '/' }));
        }
      })
    );
  }
}
