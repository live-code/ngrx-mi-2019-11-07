// app/core/auth/auth.ts
export interface Auth {
  accessToken: string;
}

export interface Credentials {
  email: string;
  password: string;
}
