// app/core/auth/auth.effects
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType, ROOT_EFFECTS_INIT } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, exhaustMap, filter, map, mapTo, switchMapTo, tap } from 'rxjs/operators';
import * as AuthActions from './auth.actions';
import { Auth } from './auth';
import { AuthService } from './auth.service';
import * as RouterActions from '../store/actions/router.actions';

@Injectable({ providedIn: 'root' })
export class AuthEffects {
  initEffect$ = createEffect(() => this.actions$.pipe(
    ofType(ROOT_EFFECTS_INIT),
    // get token
    mapTo(this.authService.getToken()),
    // we want dispatch an action only when an accessToken is found in localStorage
    filter(accessToken => !!accessToken),
    // save token in localStorage
    map(accessToken => AuthActions.saveAuth({ auth: { accessToken }}))
  ));


  loginEffect$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthActions.login),
      exhaustMap(action =>
        this.authService.login(action.email, action.password).pipe(
          map((auth: Auth) => AuthActions.loginSuccess({auth})),
          catchError(() => of(AuthActions.loginFailed())),
        )
      )
    )
  );

  loginSuccessEffect$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthActions.loginSuccess),
      tap(action => this.authService.saveAuth(action.auth)),
      switchMapTo([RouterActions.go({path: 'users'})]),
    ),
  );

  logout$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthActions.logout),
      tap(action => this.authService.cleanAuth()),
      switchMapTo([
        RouterActions.go({path: '/'}),
        AuthActions.logoutSuccess()
      ]),
    )
  );

  constructor(
    private actions$: Actions,
    private authService: AuthService,
  ) {
  }
}
