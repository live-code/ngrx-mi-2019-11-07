// app/core/auth/iflogged.component.ts
// app/core/components/navbar.component.ts
import { Directive, OnDestroy, OnInit, TemplateRef, ViewContainerRef } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Subject } from 'rxjs';
import { distinctUntilChanged, map, takeUntil } from 'rxjs/operators';
import { AppState } from '../../app.module';
import { getToken } from './auth.selector';

@Directive({
  selector: '[appIfLogged]'
})
export class IfloggedDirective implements OnInit, OnDestroy {

  private destroy$ = new Subject();

  constructor(
    private template: TemplateRef<any>,
    private view: ViewContainerRef,
    private store: Store<AppState>,
  ) {
  }

  ngOnInit() {
    this.store
      .pipe(
        select(getToken),
        map(token => !!token),
        distinctUntilChanged(),
        takeUntil(this.destroy$)
      )
      .subscribe(isLogged => {
        if (isLogged) {
          this.view.createEmbeddedView(this.template);
        } else if (!isLogged) {
          this.view.clear();
        }
      });
  }

  ngOnDestroy() {
    this.destroy$.complete();
  }
}
