import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarComponent } from './components/navbar.component';
import { RouterModule } from '@angular/router';
import { IfloggedDirective } from './auth/iflogged.directive';

@NgModule({
  imports: [
    CommonModule,
    RouterModule
  ],
  declarations: [NavbarComponent, IfloggedDirective],
  exports: [NavbarComponent, IfloggedDirective]
})
export class CoreModule { }
