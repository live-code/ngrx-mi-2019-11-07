import { createReducer, on } from '@ngrx/store';
import { updateTheme } from '../actions/settings.actions';
import { AppState } from '../../../app.module';

export interface SettingsState {
  theme: string;
}

const initialState: SettingsState = {
  theme: 'dark',
};

export const settingsReducer = createReducer(
  initialState,
  on(updateTheme, (state, action) => ({ theme: action.theme}) )
);


export const selectTheme = (state: AppState) => state.settings.theme;
