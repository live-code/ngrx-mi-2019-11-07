import { createAction, props } from '@ngrx/store';

export const go = createAction(
  '[ROUTER] go',
  props<{ path: string }>()
);

export const back = createAction('[ROUTER] back');
export const forward = createAction('[ROUTER] forward');
