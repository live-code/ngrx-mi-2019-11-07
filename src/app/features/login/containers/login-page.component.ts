import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../../../app.module';
import { login } from '../../../core/auth/auth.actions';
import { Credentials } from '../../../core/auth/auth';

@Component({
  selector: 'app-login-page',
  template: `
    <form #f="ngForm" (submit)="signin(f.value)" class="container mt-5" style="max-width: 400px;">
      <h2>Login</h2>

      <div class="alert alert-danger" *ngIf="false">
        Wrong credentials
      </div>

      <div class="form-group">
        <input type="text"
               placeholder="email"
               [ngModel] name="email" class="form-control" required>
      </div>
      <div class="form-group">
        <input type="password"
               placeholder="password" [ngModel] name="password" class="form-control" required>
      </div>

      <button type="submit" class="btn btn-primary btn-block btn-lg">
        <i class="fas fa-sign-in-alt"></i> SIGN IN</button>
    </form>
  `,
  styles: []
})
export class LoginPageComponent {

  constructor(private store: Store<AppState>) {}

  signin({ email, password }) {
    this.store.dispatch(login({ email, password }));
  }
}
