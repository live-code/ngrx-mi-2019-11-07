import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SettingsPageComponent } from './containers/settings-page.component';   // NEW

const routes: Routes = [
  { path: '', component: SettingsPageComponent}                                 // NEW
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SettingsRoutingModule { }
