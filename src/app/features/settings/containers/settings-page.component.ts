import { Component, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { updateTheme } from '../../../core/store/actions/settings.actions';
import { AppState } from '../../../app.module';
import { Observable } from 'rxjs';
import { selectTheme, SettingsState } from '../../../core/store/reducers/settings.reducer';

@Component({
  selector: 'app-settings-page',
  template: `
      
    <ng-container *ngIf="(theme$ | async) as theme">
      <button 
        [ngClass]="{
          'bg-warning': theme === 'light'  
        }" 
        (click)="setThemeHandler('light')"
      >light</button>
      
      <button 
        [ngClass]="{
          'bg-warning': theme === 'dark'  
        }" 
        (click)="setThemeHandler('dark')"
      >Dark</button>
    </ng-container>
  `,
  styles: []
})
export class SettingsPageComponent {
  theme$: Observable<string> = this.store.pipe(select(selectTheme));

  constructor(private store: Store<AppState>) {}

  setThemeHandler(theme: string) {
    this.store.dispatch(updateTheme({ theme }));
  }
}
