import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsersRoutingModule } from './users-routing.module';
import { UsersPageComponent } from './containers/users-page.component';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { UsersEffects } from './store/effects/users.effects';
import { UsersService } from './services/users.service';
import { FormsModule } from '@angular/forms';
import { reducers } from './store/reducers';


@NgModule({
  declarations: [UsersPageComponent],
  imports: [
    CommonModule,
    UsersRoutingModule,
    FormsModule,
    StoreModule.forFeature('users', reducers),
    EffectsModule.forFeature([ UsersEffects ])
  ],
  providers: [
    UsersEffects,
    UsersService
  ]
})
export class UsersModule { }
