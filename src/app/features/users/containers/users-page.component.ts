import { Component, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { cleanActiveUser, deleteUser, loadUsers, saveUser, setActiveUser } from '../store/actions/users.actions';
import { Observable } from 'rxjs';
import { User } from '../model/user';
import { NgForm } from '@angular/forms';
import { UsersState } from '../store/reducers';
import { getActive, getAllFilteredUsers, getAllUsers } from '../store/selectors/users.selectors';
import { filterByName } from '../store/actions/users-filter.actions';
import { go } from '../../../core/store/actions/router.actions';

@Component({
  selector: 'app-users-page',
  template: `
    
    <form #f="ngForm" (submit)="saveHandler(f)">
      <input type="text" [ngModel]="activeUser.name" name="name">
      <button type="submit">SAVE</button>
      <button type="button" (click)="resetHandler(f)">RESET</button>
    </form>
    
    
    <input
      type="text" 
      #filterName 
      (input)="setFilterHandler(filterName.value)"
    >
    
    <li
      class="list-group-item"
      (click)="setActive(user)"
      [ngClass]="{'active': user.id === activeUser.id}"
      *ngFor="let user of (users$ | async)"
    >
      {{user.name}}
      
      <div class="pull-right">
        <i class="fa fa-trash" 
           (click)="deleteHandler(user.id, $event)"></i>
      </div>
    </li>
  `,
  styles: []
})
export class UsersPageComponent {
  users$: Observable<User[]> = this.store.pipe(select(getAllFilteredUsers))
  activeUser: User;

  constructor(private store: Store<UsersState>) {
    this.store.dispatch(loadUsers());

    this.store.pipe(
      select(getActive)
    ).subscribe(val => this.activeUser = val);
  }

  deleteHandler(id: number, event: MouseEvent) {
    event.stopPropagation();
    this.store.dispatch(deleteUser({ id }));
  }

  saveHandler(form: NgForm) {
    const user: User = {
      ...this.activeUser,
      ...form.value
    };

    this.store.dispatch(saveUser({ user }));
  }

  setActive(user: User) {
    this.store.dispatch(setActiveUser({user}));
  }

  resetHandler(f: NgForm) {
    f.reset();

    this.store.dispatch(cleanActiveUser());

  }

  setFilterHandler(value: string) {
    this.store.dispatch(filterByName({ value }))
  }
}
