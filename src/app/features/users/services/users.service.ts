import { User } from '../model/user';
import { Actions } from '@ngrx/effects';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';

const API = 'http://localhost:3000';

@Injectable()
export class UsersService {
  constructor(private http: HttpClient) {}

  loadUsers(): Observable<Array<User>> {
    return this.http.get<User[]>(`${API}/users`);
  }

  deleteUser(id: number): Observable<any> {
    return this.http.delete(`${API}/users/${id}`);
  }

  addUser(user: Partial<User>): Observable<User> {
    return this.http.post<User>(`${API}/users`, user);
  }

  editUser(user: User): Observable<User> {
    return this.http.patch<User>(`${API}/users/${user.id}`, user);
  }
}
