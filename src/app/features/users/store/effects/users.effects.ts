import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import * as UsersActions from '../actions/users.actions';
import { catchError, map, switchMap } from 'rxjs/operators';
import { User } from '../../model/user';
import { of } from 'rxjs';
import { UsersService } from '../../services/users.service';

@Injectable()
export class UsersEffects {

  loadUsers$ = createEffect(() => this.actions$.pipe(
    ofType(UsersActions.loadUsers),
    switchMap(
      () => this.usersService.loadUsers()
        .pipe(
          map(users => UsersActions.loadUsersSuccess({ users })),
          catchError(() => of(UsersActions.loadUsersFailed()))
        )
    )
  ));

  deleteUser$ = createEffect(() => this.actions$.pipe(
    ofType(UsersActions.deleteUser),
    switchMap(
      (action) => this.usersService.deleteUser(action.id)
        .pipe(
          map(() => UsersActions.deleteUserSuccess({id: action.id})),
          catchError(() => of(UsersActions.deleteUserFailed()))
        )
    )
  ));

  saveUser$ = createEffect(() => this.actions$.pipe(
    ofType(UsersActions.saveUser),
    switchMap(action => {
      if (action.user.id) {
        return of(UsersActions.editUser({ user: action.user }));
      } else {
        return of(UsersActions.addUser({ user: action.user }));
      }
    })
  ));


  addUser$ = createEffect(() => this.actions$.pipe(
    ofType(UsersActions.addUser),
    switchMap(
      (action) => this.usersService.addUser(action.user)
        .pipe(
          map((user: User) => UsersActions.addUserSuccess({ user })),
          catchError(() => of(UsersActions.addUserFailed()))
        )
    )
  ));




  editUser$ = createEffect(() => this.actions$.pipe(
    ofType(UsersActions.editUser),
    switchMap(
      (action) => this.usersService.editUser(action.user)
        .pipe(
          map((user: User) => UsersActions.editUserSuccess({ user })),
          catchError(() => of(UsersActions.editUserFailed()))
        )
    )
  ));



  constructor(
    private actions$: Actions,
    private usersService: UsersService
  ) {}
}
