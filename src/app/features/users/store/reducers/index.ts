import { ActionReducerMap } from '@ngrx/store';
import { reducer as usersReducer } from './users.reducer';
import { reducer as userReducer } from './user.reducer';
import { User } from '../../model/user';
import { reducer as filtersReducer, UsersFiltersState } from './users-filter.reducer';

export interface UsersState {
  list: User[];
  active: User;
  filters: UsersFiltersState;
}
export const reducers: ActionReducerMap<UsersState> = {
  list: usersReducer,
  active: userReducer,
  filters: filtersReducer
};
