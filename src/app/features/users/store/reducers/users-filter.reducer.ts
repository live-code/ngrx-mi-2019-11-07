// features/users/store/reducers/users-filters.reducer.ts
import { createReducer, on } from '@ngrx/store';
import { filterByName } from '../actions/users-filter.actions';

export const initialState = {
  name: '',
  age: 0
};

export interface UsersFiltersState {
  name: string;
  age: number;
}

export const reducer = createReducer(
  initialState,
  on(filterByName, (state, action) => ({ ...state, name: action.value}))
);
