import { createReducer, on } from '@ngrx/store';
import * as UsersActions from '../actions/users.actions';
import { User } from '../../model/user';

export const initialState: User[] = [];

export const reducer = createReducer(
  initialState,
  on(UsersActions.loadUsersSuccess, (state, action) => [...action.users]),
  on(UsersActions.deleteUserSuccess, (state, action) => state.filter(u => u.id !== action.id)),
  on(UsersActions.addUserSuccess, (state, action) => [...state, action.user]),
  on(UsersActions.editUserSuccess, (state, action) => state.map(user => {
    return user.id === action.user.id ? action.user : user;
  }))
);
