import { createReducer, on } from '@ngrx/store';
import { User } from '../../model/user';
import * as UsersActions from '../actions/users.actions';


export const initialState: User = {};

export const reducer = createReducer(
  initialState,
  on(UsersActions.setActiveUser, (state, action) => ({...action.user})),
  on(UsersActions.deleteUserSuccess, (state, action) => {
    return state.id === action.id ? {} : state;
  }),
  on(UsersActions.addUserSuccess, (state, action) => ({})),
  on(UsersActions.cleanActiveUser, (state, action) => ({})),
);

