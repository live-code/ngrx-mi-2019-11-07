// features/users/store/actions/users-filters.actions.ts
import { createAction, props } from '@ngrx/store';

export const filterByName = createAction(
  '[User] filter by name',
  props<{value: string}>()
);
