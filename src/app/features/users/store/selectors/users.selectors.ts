import { UsersState } from '../reducers';
import { createFeatureSelector, createSelector } from '@ngrx/store';
import { User } from '../../model/user';
import { UsersFiltersState } from '../reducers/users-filter.reducer';

export const getUsersFeature = createFeatureSelector<UsersState>('users')

export const getAllUsers = createSelector(
  getUsersFeature,
  (state: UsersState) => state.list
)

export const getActive = createSelector(
  getUsersFeature,
  (state: UsersState) => state.active
);

export const getFilters = createSelector(
  getUsersFeature,
  (state: UsersState) => state.filters
);


// select all users with filters
export const getAllFilteredUsers = createSelector(
  getAllUsers,
  getFilters,
  (users: User[], filters: UsersFiltersState) => users.filter(u => {
    return u.name.toLowerCase().indexOf(filters.name.toLowerCase()) !== -1;
  })
);

