import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CoreModule } from './core/core.module';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { settingsReducer, SettingsState } from './core/store/reducers/settings.reducer';
import { EffectsModule } from '@ngrx/effects';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { routerReducer, RouterState, StoreRouterConnectingModule } from '@ngrx/router-store';
import { RouterEffects } from './core/store/effects/router.effects';
import { authReducer, AuthState } from './core/auth/auth.reducer';
import { AuthEffects } from './core/auth/auth.effects';
import { UsersService } from './features/users/services/users.service';
import { AuthInterceptor } from './core/auth/auth.interceptor';

export interface AppState {
  settings: SettingsState;
  router: RouterState;
  auth: AuthState;
}

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CoreModule,
    HttpClientModule,
    StoreModule.forRoot({
      settings: settingsReducer,
      router: routerReducer,
      auth: authReducer
    }),
    StoreDevtoolsModule.instrument({
      maxAge: 25
    }),
    StoreRouterConnectingModule.forRoot({
      routerState: RouterState.Minimal
    }),
    EffectsModule.forRoot([ AuthEffects, RouterEffects ])
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
